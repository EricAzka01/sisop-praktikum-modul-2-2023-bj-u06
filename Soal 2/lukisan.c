
 #include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <signal.h>

#include <time.h>
//#include <syslog.h>
//#include <sys/types.h>
//#include <sys/wait.h>
//#include <sys/stat.h>
//#include  <sys/ipc.h>
//#include  <sys/shm.h>

int mode(int sid){

//while(1){
        pid_t cid;
        int status;

        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        char namaFolder[64];
        strftime(namaFolder, 64, "%Y-%m-%d_%H:%M:%S", tm);

        cid = fork();
        printf("FORK CID=========================================================>: %d\n", cid);
        if(cid < 0){
            exit(EXIT_FAILURE);
        }

        if(cid == 0){
            if(fork()==0){
                printf("FORK MKDIR=========================================================>: %d\n", fork());
                printf("NAME MKDIR=========================================================>: %s\n", namaFolder);
                char *argvmk[] = {"mkdir", "-p", namaFolder, NULL};
                execv("/bin/mkdir", argvmk);

            } else{


                //while((wait(&status)) > 0);

                printf("START LOOP FOR 15 SID=========================================================>: %d\n", sid);
                for(int i=0; i<15; i++){
                    if(fork()==0){
                        if(chdir(namaFolder) < 0){
                            exit(EXIT_FAILURE);
                        }

                        int cnt;
                        cnt = (int) time(NULL);
                        cnt = (cnt % 1000) + 100;
                        time_t tcount = time(NULL);
                        struct tm* tmcount = localtime(&tcount);

                        char alamatDownload[48], namaFile[64];
                        sprintf(alamatDownload, "https://picsum.photos/%d", cnt);
                        strftime(namaFile, 64, "%Y-%m-%d+%H:%M:%S", tmcount);

                        char *argvv[] = {"/usr/bin/wget", "-P",namaFolder, alamatDownload, (char *) 0 };
                        //char *argvv[] = {"wget", alamatDownload, "-q", namaFile, NULL};
                        execv("/usr/bin/wget", argvv);
                    }
                    sleep(5);
                }
                printf("END LOOP FOR 15 SID=========================================================>: %d\n", sid);

                char namaFolderZIP[64];
                sprintf(namaFolderZIP, "%s.zip", namaFolder);

                char *argvzip[] = {"zip", "-qrm", namaFolderZIP, namaFolder, NULL};
                execv("/usr/bin/zip", argvzip);
                printf("ZIP SID=========================================================>: %d\n", sid);

               //START/KILL=================
                printf("Kill PROCESS CID===================================================================>: %d\n", sid);
                //sleep(5);
                kill(sid,9);
                exit (0);
                //END/KILL=================


            }
        }

    //}


 return 0;
}



 int main(int argc, char *argv[]){

    printf( "argc = %d\n", argc );
    for( int i = 0; i < argc; ++i ) {
        printf( "argv[ %d ] ===================================================> %s\n", i, argv[ i ] );
    }

    pid_t pid, sid;
    pid = fork();
    printf("FORK PID=========================================================>: %d\n", pid);
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid > 0){
        exit(EXIT_SUCCESS);
    }
    sid = setsid();

    printf("Process START SID=========================================================>: %d\n", sid);

    printf("ARGC ARGUMENT=========================================================>: %d\n", argc);
    while(argc){

        printf("Process mode SID=========================================================>: %d\n", mode(sid));

        sleep(30);
        printf("SLEEP 30 SID=========================================================>: %d\n", sid);
    }

    exit (0);
    return 0;
}
