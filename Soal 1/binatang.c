#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

void download(char *link, char *fname) {
    pid_t child_id;
    child_id = fork();    
    if (child_id == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", "--no-check-certificate", link, "-o", fname, NULL);
        exit(0);
    } 
    else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void unzip() {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        char *argv[] = {"unzip", "file.zip", "-d", "/home/talitha/modul2/hewan/", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    while (wait(&status) > 0);
}

void create_folder(char *kategori) {
    char Dir[100] = "/talitha/modul2/hewan/";
    strcat(Dir, kategori);

    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", Dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void movePos(const char *kategori, char *filename) {
    char file_path[200];
    strcpy(file_path, "/home/talitha/modul2/hewan/");
    strcat(file_path, filename);

    char dest_path[200];
    strcpy(dest_path, "/home/talitha/modul2/hewan/");
    strcat(dest_path, kategori);
    strcat(dest_path, filename);

    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mv", file_path, dest_path, NULL};
        execv("/bin/mv", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}


void print_random_file() {
    DIR *dp = opendir("/home/talitha/modul2/hewan/");
    struct dirent *ep;

    if (dp != NULL) {
        int num_files = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                num_files++;
            }
        }

        if (num_files == 0) {
            printf("No files found in directory.\n");
            closedir(dp);
            return;
        }

        srand(time(NULL));
        int random_num = rand() % num_files;

        rewinddir(dp);
        int count = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                if (count == random_num) {
                    char* filename = ep->d_name;
                    printf("\nShift: %s\n", filename);
                    break;
                }
                count++;
            }
        }
    }
    closedir(dp);
}

void managePos() {
    struct dirent *ep;
    DIR *dp = opendir("/home/talitha/modul2/hewan");
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            if (strstr(ep->d_name, ".jpg")) {
                if (strstr(ep->d_name, "darat") != NULL){
                        movePos("/HewanDarat", ep->d_name);
                }
                else if (strstr(ep->d_name, "amphibi") != NULL){
                        movePos("/HewanAmphibi", ep->d_name);
                }
                else if (strstr(ep->d_name, "air") != NULL){
                        movePos("/HewanAir", ep->d_name);
                }
                else{
                        movePos("/Lainnya", ep->d_name);
                }
            }
        }
    }

    closedir(dp);
}

void zip (char *folderName, char *zipName) {
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"zip", "-rm", zipName, folderName, NULL};
        execv("/usr/bin/zip", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void zip_directories () {
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        execlp("zip","zip","-r","hewan.zip","HewanDarat.zip","HewanAmphibi.zip","HewanAir.zip",NULL);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}



int main() {
    download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "hewan.zip");

    create_folder("HewanDarat");
    create_folder("HewanAmphibi");
    create_folder("HewanAir");

    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            char *argv[] = {"mkdir","-p","/home/talitha/modul2/hewan", NULL};
            execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);

    //child 1
    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            unzip();
    }
    //wait for child 1
    while ((wait(&status)) > 0);

    //child 2
    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            print_random_file();
            exit(0);
    }
    //wait for child 2
    while ((wait(&status)) > 0);  

     //child 3
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        managePos();
        exit(0);
    }
    //wait for child 3
    while ((wait(&status)) > 0);  

    //child 4
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        zip("/home/talitha/modul2/hewan/HewanDarat", "HewanDarat.zip");
        zip("/home/talitha/modul2/hewan/HewanAmphibi", "HewanAmphibi.zip");
        zip("/home/talitha/modul2/hewan/HewanAir", "HewanAir.zip");
        exit(0);
    }
    //wait for child 4
    while ((wait(&status)) > 0);

    //child 5
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        zip_directories();
        exit(0);
    }

    //wait for child 5
    while ((wait(&status)) > 0);  
  
}
