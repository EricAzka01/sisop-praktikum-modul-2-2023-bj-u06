#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/types.h>

void move_files(const char * search_pattern,
  const char * destination) {
  if (fork() == 0) {
    chdir("./players");
    execl("/usr/bin/find", "find", ".", "-name", search_pattern, "-exec", "mv", "{}", destination, ";", NULL);
    printf("Moving files failed.\n");
    exit(1);
  }
}

typedef struct {
  char filename[300];
  int rating;
}
Player;

int compare_rating(const void * a,
  const void * b) {
  return ((Player * ) b) -> rating - ((Player * ) a) -> rating;
}

void read_players(const char * position, Player * players, int * count) {
  DIR * dir;
  struct dirent * entry;
  char directory[100];

  sprintf(directory, "./players/%s", position);
  dir = opendir(directory);

  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG && strstr(entry -> d_name, ".png") != NULL) {
        int rating;
        sscanf(entry -> d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", & rating);
        strcpy(players[ * count].filename, entry -> d_name);
        players[ * count].rating = rating;
        ( * count) ++;
      }
    }
    closedir(dir);
  } else {
    printf("Cannot open '%s' directory.\n", directory);
    exit(1);
  }
}

void buatTim(int bek, int gelandang, int penyerang) {
  Player kiper[1], bek_players[100], gelandang_players[100], penyerang_players[100];
  int kiper_count = 0, bek_count = 0, gelandang_count = 0, penyerang_count = 0;

  read_players("Kiper", kiper, & kiper_count);
  read_players("Bek", bek_players, & bek_count);
  read_players("Gelandang", gelandang_players, & gelandang_count);
  read_players("Penyerang", penyerang_players, & penyerang_count);

  qsort(bek_players, bek_count, sizeof(Player), compare_rating);
  qsort(gelandang_players, gelandang_count, sizeof(Player), compare_rating);
  qsort(penyerang_players, penyerang_count, sizeof(Player), compare_rating);

  char filepath[300];
  sprintf(filepath, "/home/eric/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE * file = fopen(filepath, "w");

  if (file != NULL) {
    fprintf(file, "Kiper:\n%s\n\n", kiper[0].filename);
    fprintf(file, "Bek:\n");
    for (int i = 0; i < bek && i < bek_count; i++) {
      fprintf(file, "%s\n", bek_players[i].filename);
    }
    fprintf(file, "\nGelandang:\n");
    for (int i = 0; i < gelandang && i < gelandang_count; i++) {
      fprintf(file, "%s\n", gelandang_players[i].filename);
    }
    fprintf(file, "\nPenyerang:\n");
    for (int i = 0; i < penyerang && i < penyerang_count; i++) {
      fprintf(file, "%s\n", penyerang_players[i].filename);
    }

    fclose(file);
    printf("'Kesebelasan Terbaik' formation saved to %s\n", filepath);
  } else {
    printf("Error creating lineup file.\n");
    exit(1);
  }
}
void download(){
    char* args[] = { "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL };
    if (fork() == 0) execvp("wget", args);
    else wait(NULL);
}

void extract(){
    char* args[] = { "unzip", "players.zip", NULL };
    if (fork() == 0) execvp("unzip", args);
    else wait(NULL);
}

void delete(){
    char* args[] = { "rm", "players.zip", NULL };
    if (fork() == 0) execvp("rm", args);
    else wait(NULL);
}

      

int main() {

    download();

    extract();

    delete();

  DIR * dir;
  struct dirent * entry;

  dir = opendir("./players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG && strstr(entry -> d_name, ".png") != NULL) {
        if (strstr(entry -> d_name, "_ManUtd_") == NULL) {
          char file[300];
          sprintf(file, "./players/%s", entry -> d_name);
          remove(file);
        }
      }
    }
    closedir(dir);
  } else {
    printf("Cannot open 'players' directory.\n");
    return 1;
  }


  pid_t pid = fork();
  if (pid == 0) {
    execl("/bin/mkdir", "mkdir", "-p", "./players/Kiper", "./players/Bek", "./players/Gelandang", "./players/Penyerang", NULL);
    printf("Creating directory failed.\n");
    exit(1);
  } else {
    int status;
    waitpid(pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Creating directory failed.\n");
      exit(1);
    }
  }

  move_files("*_Bek_*", "./Bek");
  move_files("*_Gelandang_*", "./Gelandang");
  move_files("*_Kiper_*", "./Kiper");
  move_files("*_Penyerang_*", "./Penyerang");

  int status;
  for (int i = 0; i < 4; i++) {
    wait( & status);
    if (WEXITSTATUS(status) != 0) {
      printf("Moving files failed.\n");
      exit(1);
    } else {
      printf("3c. Players categorized successfully.\n");
    }
  }

  buatTim(4, 3, 3); 

  return 0;
}
