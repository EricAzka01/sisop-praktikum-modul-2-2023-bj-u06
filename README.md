# sisop-praktikum-modul-2-2023-bj-u06

Group Members:

- Eric Azka Nugroho [5025211064]
- Kirana Alivia Enrico [5025211190]
- Talitha Hayyinas Sahala [5025211263]

## Question 1

Grape-kun adalah seorang penjaga hewan di kebun binatang, dia mendapatkan tugas dari atasannya untuk melakukan penjagaan pada beberapa hewan-hewan yang ada di kebun binatang sebelum melakukan penjagaan Grape-kun harus mengetahui terlebih dahulu hewan apa aja yang harus dijaga dalam drive kebun binatang tersebut terdapat folder gambar dari hewan apa saja yang harus dijaga oleh Grape-kun. Berikut merupakan link download dari drive kebun binatang tersebut : https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq 

- Grape-kun harus mendownload file tersebut untuk disimpan pada penyimpanan local komputernya. Dan untuk melakukan melihat file gambar pada folder yang telah didownload Grape-kun harus melakukan unzip pada folder tersebut.


- Setelah berhasil melakukan unzip Grape-kun melakukan pemilihan secara acak pada file gambar tersebut untuk melakukan shift penjagaan pada hewan tersebut.

- Karena Grape-kun adalah orang yang perfeksionis Grape-kun ingin membuat direktori untuk memilah file gambar tersebut. Direktori tersebut dengan nama HewanDarat, HewanAmphibi, dan HewanAir. Setelah membuat direktori tersebut Grape-kun harus melakukan filter atau pemindahan file gambar hewan sesuai dengan tempat tinggal nya.

- Setelah mengetahui hewan apa saja yang harus dijaga Grape-kun melakukan zip kepada direktori yang dia buat sebelumnya agar menghemat penyimpanan.

Catatan : 
untuk melakukan zip dan unzip tidak boleh menggunakan system

binatang.c 
**CODE**

```
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

void download(char *link, char *fname) {
    pid_t child_id;
    child_id = fork();    
    if (child_id == 0) {
        execl("/usr/bin/wget", "/usr/bin/wget", "--no-check-certificate", link, "-o", fname, NULL);
        exit(0);
    } 
    else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void unzip() {
    pid_t child_id;
    int status;
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        char *argv[] = {"unzip", "file.zip", "-d", "/home/talitha/modul2/hewan/", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    while (wait(&status) > 0);
}

void create_folder(char *kategori) {
    char Dir[100] = "/talitha/modul2/hewan/";
    strcat(Dir, kategori);

    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"mkdir", "-p", Dir, NULL};
        execv("/bin/mkdir", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void movePos(const char *kategori, char *filename) {
    char file_path[200];
    strcpy(file_path, "/home/talitha/modul2/hewan/");
    strcat(file_path, filename);

    char dest_path[200];
    strcpy(dest_path, "/home/talitha/modul2/hewan/");
    strcat(dest_path, kategori);
    strcat(dest_path, filename);

    pid_t child_id;
    child_id = fork();

    if (child_id == 0) {
        char *argv[] = {"mv", file_path, dest_path, NULL};
        execv("/bin/mv", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}


void print_random_file() {
    DIR *dp = opendir("/home/talitha/modul2/hewan/");
    struct dirent *ep;

    if (dp != NULL) {
        int num_files = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                num_files++;
            }
        }

        if (num_files == 0) {
            printf("No files found in directory.\n");
            closedir(dp);
            return;
        }

        srand(time(NULL));
        int random_num = rand() % num_files;

        rewinddir(dp);
        int count = 0;
        while ((ep = readdir(dp)) != NULL) {
            if (ep->d_type == DT_REG) {
                if (count == random_num) {
                    char* filename = ep->d_name;
                    printf("\nShift: %s\n", filename);
                    break;
                }
                count++;
            }
        }
    }
    closedir(dp);
}

void managePos() {
    struct dirent *ep;
    DIR *dp = opendir("/home/talitha/modul2/hewan");
    if (dp != NULL) {
        while ((ep = readdir(dp))) {
            if (strstr(ep->d_name, ".jpg")) {
                if (strstr(ep->d_name, "darat") != NULL){
                        movePos("/HewanDarat", ep->d_name);
                }
                else if (strstr(ep->d_name, "amphibi") != NULL){
                        movePos("/HewanAmphibi", ep->d_name);
                }
                else if (strstr(ep->d_name, "air") != NULL){
                        movePos("/HewanAir", ep->d_name);
                }
                else{
                        movePos("/Lainnya", ep->d_name);
                }
            }
        }
    }

    closedir(dp);
}

void zip (char *folderName, char *zipName) {
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        char *argv[] = {"zip", "-rm", zipName, folderName, NULL};
        execv("/usr/bin/zip", argv);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}

void zip_directories () {
    pid_t child_id;
    child_id = fork();
    if (child_id == 0) {
        execlp("zip","zip","-r","hewan.zip","HewanDarat.zip","HewanAmphibi.zip","HewanAir.zip",NULL);
        exit(0);
    } else {
        int status;
        waitpid(child_id, &status, 0);
    }
}



int main() {
    download("https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq", "hewan.zip");

    create_folder("HewanDarat");
    create_folder("HewanAmphibi");
    create_folder("HewanAir");

    pid_t child_id;
    int status;

    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            char *argv[] = {"mkdir","-p","/home/talitha/modul2/hewan", NULL};
            execv("/bin/mkdir", argv);
    }
    while ((wait(&status)) > 0);

    //child 1
    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            unzip();
    }
    //wait for child 1
    while ((wait(&status)) > 0);

    //child 2
    child_id = fork();
    if (child_id < 0) {
            exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
            print_random_file();
            exit(0);
    }
    //wait for child 2
    while ((wait(&status)) > 0);  

     //child 3
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        managePos();
        exit(0);
    }
    //wait for child 3
    while ((wait(&status)) > 0);  

    //child 4
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }
    if (child_id == 0) {
        zip("/home/talitha/modul2/hewan/HewanDarat", "HewanDarat.zip");
        zip("/home/talitha/modul2/hewan/HewanAmphibi", "HewanAmphibi.zip");
        zip("/home/talitha/modul2/hewan/HewanAir", "HewanAir.zip");
        exit(0);
    }
    //wait for child 4
    while ((wait(&status)) > 0);

    //child 5
    child_id = fork();
    if (child_id < 0) {
        exit(EXIT_FAILURE);
    }

    if (child_id == 0) {
        zip_directories();
        exit(0);
    }

    //wait for child 5
    while ((wait(&status)) > 0);  
  
}


```
**Explaination**

In this problem, we were tasked to write a program with c language for several requirements first is we have to download the file to be stored on the computer's local storage, here we downloads a file using wget command, with the input arguments link (source URL) and fname (output file name). after successfully downloading the file we have to unzip the folder to see the image files in the folder that has been downloaded we used to extracts the downloaded zip file ("hewan.zip") to the "/home/talitha/modul2/hewan/" directory.
After successfully unzipping we creates a new folder with the input argument kategori (category) namely, HewanDarat, HewanAmphibi, and HewanAir, which is concatenated to the base directory path "/talitha/modul2/hewan/".And moves a file from one directory to another. Takes two input arguments: kategori (destination directory) and filename (the file to move).then, Prints the name of a random file from the "/home/talitha/modul2/hewan/" directory. managePos functions is for Moves the files with the ".jpg" extension to the corresponding category folders (HewanDarat, HewanAmphibi, HewanAir) based on the presence of specific substrings in their file names.then we zips the contents of a folder and saves it as a zip file. Takes two input arguments: folderName (the folder to zip) and zipName (the output zip file name). the last we zips the contents of the HewanDarat, HewanAmphibi, and HewanAir folders into a single zip file named "hewan.zip".


## Question 2

Sucipto adalah seorang seniman terkenal yang berasal dari Indonesia. Karya nya sudah terkenal di seluruh dunia, dan lukisannya sudah dipajang di berbagai museum mancanegara. Tetapi, akhir-akhir ini sucipto sedang terkendala mengenai ide lukisan ia selanjutnya. Sebagai teman yang jago sisop, bantu sucipto untuk melukis dengan mencarikannya gambar-gambar di internet sebagai referensi !

- Pertama-tama, buatlah sebuah folder khusus, yang dalamnya terdapat sebuah program C yang per 30 detik membuat sebuah folder dengan nama timestamp [YYYY-MM-dd_HH:mm:ss].

- Tiap-tiap folder lalu diisi dengan 15 gambar yang di download dari https://picsum.photos/ , dimana tiap gambar di download setiap 5 detik. Tiap gambar berbentuk persegi dengan ukuran (t%1000)+50 piksel dimana t adalah detik Epoch Unix. Gambar tersebut diberi nama dengan format timestamp [YYYY-mm-dd_HH:mm:ss].

- Agar rapi, setelah sebuah folder telah terisi oleh 15 gambar, folder akan di zip dan folder akan di delete(sehingga hanya menyisakan .zip).

- Karena takut program tersebut lepas kendali, Sucipto ingin program tersebut men-generate sebuah program "killer" yang siap di run(executable) untuk menterminasi semua operasi program tersebut. Setelah di run, program yang menterminasi ini lalu akan mendelete dirinya sendiri.

- Buatlah program utama bisa dirun dalam dua mode, yaitu MODE_A dan MODE_B. untuk mengaktifkan MODE_A, program harus dijalankan dengan argumen -a. Untuk MODE_B, program harus dijalankan dengan argumen -b. Ketika dijalankan dalam MODE_A, program utama akan langsung menghentikan semua operasinya ketika program killer dijalankan. Untuk MODE_B, ketika program killer dijalankan, program utama akan berhenti tapi membiarkan proses di setiap folder yang masih berjalan sampai selesai(semua folder terisi gambar, terzip lalu di delete).
Catatan : 
> Tidak boleh menggunakan system()
> Proses berjalan secara daemon
> Proses download gambar pada beberapa folder dapat berjalan secara bersamaan (overlapping)

**CODE**

```

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#include <signal.h>

#include <time.h>
//#include <syslog.h>
//#include <sys/types.h>
//#include <sys/wait.h>
//#include <sys/stat.h>
//#include  <sys/ipc.h>
//#include  <sys/shm.h>

int mode(int sid){

//while(1){
        pid_t cid;
        int status;

        time_t t = time(NULL);
        struct tm* tm = localtime(&t);

        char namaFolder[64];
        strftime(namaFolder, 64, "%Y-%m-%d_%H:%M:%S", tm);

        cid = fork();
        printf("FORK CID=========================================================>: %d\n", cid);
        if(cid < 0){
            exit(EXIT_FAILURE);
        }

        if(cid == 0){
            if(fork()==0){
                printf("FORK MKDIR=========================================================>: %d\n", fork());
                printf("NAME MKDIR=========================================================>: %s\n", namaFolder);
                char *argvmk[] = {"mkdir", "-p", namaFolder, NULL};
                execv("/bin/mkdir", argvmk);

            } else{


                //while((wait(&status)) > 0);

                printf("START LOOP FOR 15 SID=========================================================>: %d\n", sid);
                for(int i=0; i<15; i++){
                    if(fork()==0){
                        if(chdir(namaFolder) < 0){
                            exit(EXIT_FAILURE);
                        }

                        int cnt;
                        cnt = (int) time(NULL);
                        cnt = (cnt % 1000) + 100;
                        time_t tcount = time(NULL);
                        struct tm* tmcount = localtime(&tcount);

                        char alamatDownload[48], namaFile[64];
                        sprintf(alamatDownload, "https://picsum.photos/%d", cnt);
                        strftime(namaFile, 64, "%Y-%m-%d+%H:%M:%S", tmcount);

                        char *argvv[] = {"/usr/bin/wget", "-P",namaFolder, alamatDownload, (char *) 0 };
                        //char *argvv[] = {"wget", alamatDownload, "-q", namaFile, NULL};
                        execv("/usr/bin/wget", argvv);
                    }
                    sleep(5);
                }
                printf("END LOOP FOR 15 SID=========================================================>: %d\n", sid);

                char namaFolderZIP[64];
                sprintf(namaFolderZIP, "%s.zip", namaFolder);

                char *argvzip[] = {"zip", "-qrm", namaFolderZIP, namaFolder, NULL};
                execv("/usr/bin/zip", argvzip);
                printf("ZIP SID=========================================================>: %d\n", sid);

               //START/KILL=================
                printf("Kill PROCESS CID===================================================================>: %d\n", sid);
                //sleep(5);
                kill(sid,9);
                exit (0);
                //END/KILL=================


            }
        }

    //}


 return 0;
}



 int main(int argc, char *argv[]){

    printf( "argc = %d\n", argc );
    for( int i = 0; i < argc; ++i ) {
        printf( "argv[ %d ] ===================================================> %s\n", i, argv[ i ] );
    }

    pid_t pid, sid;
    pid = fork();
    printf("FORK PID=========================================================>: %d\n", pid);
    if(pid < 0){
        exit(EXIT_FAILURE);
    }
    if(pid > 0){
        exit(EXIT_SUCCESS);
    }
    sid = setsid();

    printf("Process START SID=========================================================>: %d\n", sid);

    printf("ARGC ARGUMENT=========================================================>: %d\n", argc);
    while(argc){

        printf("Process mode SID=========================================================>: %d\n", mode(sid));

        sleep(30);
        printf("SLEEP 30 SID=========================================================>: %d\n", sid);
    }

    exit (0);
    return 0;
}

```

**Explaination**
This code is a C program that performs a specific task repeatedly based on the given arguments. It creates a daemon process and runs a specific function called "mode" in a loop with a sleep interval of 30 seconds. The "mode" function creates a new folder with a timestamped name, downloads 15 images from a URL using wget, compresses the folder as a ZIP file, and kills its parent process, which terminates the daemon process.

The main function checks the number of arguments passed to the program and creates a daemon process using fork and setsid system calls. It then starts the loop to run the "mode" function and sleeps for 30 seconds. The loop runs until there are no more arguments, and the program exits with status 0.

The program uses several C library functions and system calls, including fork, execv, setsid, sleep, time, localtime, strftime, chdir, sprintf, and kill. It also includes several C library headers, such as stdio.h, stdlib.h, string.h, errno.h, unistd.h, signal.h, and time.h

## Question 3

Ten Hag adalah seorang pelatih Ajax di Liga Belanda. Suatu hari, Ten Hag mendapatkan tawaran untuk menjadi manajer Manchester United. Karena Ten Hag masih mempertimbangkan tawaran tersebut, ia ingin mengenal para pemain yang akan dilatih kedepannya. Dikarenakan Ten Hag hanya mendapatkan url atau link database mentah para pemain bola, maka ia perlu melakukan klasifikasi pemain Manchester United. Bantulah Ten Hag untuk mengenal para pemain Manchester United tersebut hanya dengan 1 Program C bernama “filter.c”

- Pertama-tama, Program filter.c akan mengunduh file yang berisikan database para pemain bola. Kemudian dalam program yang sama diminta dapat melakukan extract “players.zip”. Lalu hapus file zip tersebut agar tidak memenuhi komputer Ten Hag.

- Dikarenakan database yang diunduh masih data mentah. Maka bantulah Ten Hag untuk menghapus semua pemain yang bukan dari Manchester United yang ada di directory.

- Setelah mengetahui nama-nama pemain Manchester United, Ten Hag perlu untuk mengkategorikan pemain tersebut sesuai dengan posisi mereka dalam waktu bersamaan dengan 4 proses yang berbeda. Untuk kategori folder akan menjadi 4 yaitu Kiper, Bek, Gelandang, dan Penyerang.

- Setelah mengkategorikan anggota tim Manchester United, Ten Hag memerlukan Kesebelasan Terbaik untuk menjadi senjata utama MU berdasarkan rating terbaik dengan wajib adanya kiper, bek, gelandang, dan penyerang. (Kiper pasti satu pemain). Untuk output nya akan menjadi Formasi_[jumlah bek]-[jumlah gelandang]-[jumlah penyerang].txt dan akan ditaruh di /home/[users]/
Catatan:
> Format nama file yang akan diunduh dalam zip dan isi txt formasi berupa [nama]_[tim]_[posisi]_[rating].png
> Tidak boleh menggunakan system()
> Tidak boleh memakai function C mkdir() ataupun rename().
> Gunakan exec() dan fork().
> Directory “.” dan “..” tidak termasuk yang akan dihapus.
> Untuk poin d DIWAJIBKAN membuat fungsi bernama buatTim(int, int, int), dengan input 3 value integer dengan urutan bek, gelandang, dan striker.


***CODE***

3A
```
void download(){
    char* args[] = { "wget", "-O", "players.zip", "https://drive.google.com/u/0/uc?id=1zEAneJ1-0sOgt13R1gL4i1ONWfKAtwBF&export=download", NULL };
    if (fork() == 0) execvp("wget", args);
    else wait(NULL);
}

void extract(){
    char* args[] = { "unzip", "players.zip", NULL };
    if (fork() == 0) execvp("unzip", args);
    else wait(NULL);
}

void delete(){
    char* args[] = { "rm", "players.zip", NULL };
    if (fork() == 0) execvp("rm", args);
    else wait(NULL);
}


int main() {
    
    download();

    extract();

    delete();


```
3B
```
DIR * dir;
  struct dirent * entry;

  dir = opendir("./players");
  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG && strstr(entry -> d_name, ".png") != NULL) {
        if (strstr(entry -> d_name, "_ManUtd_") == NULL) {
          char file[300];
          sprintf(file, "./players/%s", entry -> d_name);
          remove(file);
        }
      }
    }
    closedir(dir);
    printf("3b. Non-Manchester United players removed successfully.\n");
  } else {
    printf("Cannot open 'players' directory.\n");
    return 1;
  }



```

3C

```
void move_files(const char * search_pattern,
  const char * destination) {
  if (fork() == 0) {
    chdir("./players");
    execl("/usr/bin/find", "find", ".", "-name", search_pattern, "-exec", "mv", "{}", destination, ";", NULL);
    printf("Moving files failed.\n");
    exit(1);
  }
}

int main(){
  pid_t pid = fork();
  if (pid == 0) {
    execl("/bin/mkdir", "mkdir", "-p", "./players/Kiper", "./players/Bek", "./players/Gelandang", "./players/Penyerang", NULL);
    printf("Creating directory failed.\n");
    exit(1);
  } else {
    int status;
    waitpid(pid, & status, 0);
    if (WEXITSTATUS(status) != 0) {
      printf("Creating directory failed.\n");
      exit(1);
    }
  }

  move_files("*_Bek_*", "./Bek");
  move_files("*_Gelandang_*", "./Gelandang");
  move_files("*_Kiper_*", "./Kiper");
  move_files("*_Penyerang_*", "./Penyerang");

  int status;
  for (int i = 0; i < 4; i++) {
    wait( & status);
    if (WEXITSTATUS(status) != 0) {
      printf("Moving files failed.\n");
      exit(1);
    } else {
      printf("3c. Players categorized successfully.\n");
    }
  }
}

```

3D

```
typedef struct {
  char filename[300];
  int rating;
}
Player;

int compare_rating(const void * a,
  const void * b) {
  return ((Player * ) b) -> rating - ((Player * ) a) -> rating;
}

void read_players(const char * position, Player * players, int * count) {
  DIR * dir;
  struct dirent * entry;
  char directory[100];

  sprintf(directory, "./players/%s", position);
  dir = opendir(directory);

  if (dir != NULL) {
    while ((entry = readdir(dir)) != NULL) {
      if (entry -> d_type == DT_REG && strstr(entry -> d_name, ".png") != NULL) {
        int rating;
        sscanf(entry -> d_name, "%*[^_]_%*[^_]_%*[^_]_%d.png", & rating);
        strcpy(players[ * count].filename, entry -> d_name);
        players[ * count].rating = rating;
        ( * count) ++;
      }
    }
    closedir(dir);
  } else {
    printf("Cannot open '%s' directory.\n", directory);
    exit(1);
  }
}

void buatTim(int bek, int gelandang, int penyerang) {
  Player kiper[1], bek_players[100], gelandang_players[100], penyerang_players[100];
  int kiper_count = 0, bek_count = 0, gelandang_count = 0, penyerang_count = 0;

  read_players("Kiper", kiper, & kiper_count);
  read_players("Bek", bek_players, & bek_count);
  read_players("Gelandang", gelandang_players, & gelandang_count);
  read_players("Penyerang", penyerang_players, & penyerang_count);

  qsort(bek_players, bek_count, sizeof(Player), compare_rating);
  qsort(gelandang_players, gelandang_count, sizeof(Player), compare_rating);
  qsort(penyerang_players, penyerang_count, sizeof(Player), compare_rating);

  char filepath[300];
  sprintf(filepath, "/home/eric/Formasi_%d-%d-%d.txt", bek, gelandang, penyerang);
  FILE * file = fopen(filepath, "w");

  if (file != NULL) {
    fprintf(file, "Kiper:\n%s\n\n", kiper[0].filename);
    fprintf(file, "Bek:\n");
    for (int i = 0; i < bek && i < bek_count; i++) {
      fprintf(file, "%s\n", bek_players[i].filename);
    }
    fprintf(file, "\nGelandang:\n");
    for (int i = 0; i < gelandang && i < gelandang_count; i++) {
      fprintf(file, "%s\n", gelandang_players[i].filename);
    }
    fprintf(file, "\nPenyerang:\n");
    for (int i = 0; i < penyerang && i < penyerang_count; i++) {
      fprintf(file, "%s\n", penyerang_players[i].filename);
    }

    fclose(file);
    printf("3d. 'Kesebelasan Terbaik' formation saved to %s\n", filepath);
  } else {
    printf("Error creating lineup file.\n");
    exit(1);
  }
}

int main(){
    buatTim(4, 3, 3); // format: bek, gelandang, penyerang

  return 0;
}

```

Explaination

3A

For this problem, we were tasked to download, extract, and unzip a file. In this problem, we use execvp to complete our code. Execvp itself replaces the current process image with a new process image specified by file. For the download, we use execvp to download the file from the internet. We use the command "wget" is a file downloader command. For the extract, we also use execvp to unzip the file by using "unzip". Lastly, for the delete, we use execvp "rm" to remove the file from our computer. We use 3 different function to simplify the code. In the main function, we just call the function.

3B

For this problem, we were tasked to seperate the players of Manchester United and other team. We use 2 seperate if to make sure that players file format is in .png and "ManUtd". Then, after all of that is succeed, the players other than Manchester United will be deleted. We use term like strstr, readdir, and sprintf. Strstr is to compare the string of the file name. While readdir is to return a pointer to a structure representing the directory entry at the current position in the directory stream specified by the argument dirp, and positions the directory stream at the next entry. Lastly, sprintf functions formats and stores a series of characters and values in the array buffer.

3C

For this problem, we were tasked to move the players of Manchester United according to their position. This code performs the categorization of files based on their name pattern and moves them to different directories. The main function first creates four directories named "Bek", "Gelandang", "Kiper", and "Penyerang" inside the "players" directory. Then, it calls the "move_files" function four times with different parameters to categorize the files based on their position.

The "move_files" function first creates a child process using the fork() system call. The child process then changes its working directory to "./players" using the chdir() system call. It then calls the "execl" system call to execute the "find" command with the parameters "-name", the given search pattern, "-exec", "mv", "{}", and the given destination directory, followed by ";". This command searches for files with names matching the given pattern and moves them to the specified destination directory.

If the "execl" command fails, the child process prints an error message "Moving files failed" and exits with a status code of 1.

In the main function, after creating the directories using the "mkdir" command, the "move_files" function is called four times with different search patterns and destination directories. Then, a loop is used to wait for the child processes to finish and check their exit status. If any of the child processes failed, the program prints an error message "Moving files failed" and exits with a status code of 1. If all the child processes succeed, the program prints a message "Players categorized successfully" for each one. 

3D

For this problem, we were tasked to generate the Manchester United team lineup based on their ratings. The program defines a struct called Player, which has two fields: filename, which holds the name of the player's image file, and rating, which holds the player's rating.

The read_players() function reads player files from a given directory (./players/<position>/) and stores them in an array of Player structs. It only reads files with a .png extension and extracts the player's rating from the filename using the sscanf() function.

The compare_rating() function is used as a callback function for the qsort() function, which sorts the arrays of players by their ratings in descending order.

The buatTim() function generates the soccer team lineup. It calls read_players() for each position and sorts the arrays of players by rating using qsort(). It then creates a new text file and writes the names of the top players for each position to it, based on the formation specified in the function call. The file is saved in the specified filepath.

Finally, the main() function calls buatTim() with the desired formation (4 defenders, 3 midfielders, and 3 forwards) and prints a success message if the lineup file was created successfully.

## Question 4

Banabil adalah seorang mahasiswa yang rajin kuliah dan suka belajar. Namun naasnya Banabil salah mencari teman, dia diajak ke toko mainan oleh teman-temannya dan teracuni untuk membeli banyak sekali mainan dan kebingungan memilih mainan mana yang harus dibeli. Hal tersebut menyebabkan Banabil kehilangan fokus dalam pengerjaan tugas-tugas yang diberikan oleh dosen nya. Untuk mengembalikan fokusnya, Banabil harus melatih diri sendiri dalam membuat program untuk menjalankan script bash yang menyerupai crontab dan menggunakan bahasa C karena baru dipelajari olehnya.
Untuk menambah tantangan agar membuatnya semakin terfokus, Banabil membuat beberapa ketentuan custom yang harus dia ikuti sendiri. Ketentuan tersebut berupa:

- Banabil tidak ingin menggunakan fungsi system(), karena terlalu mudah.

- Dalam pelatihan fokus time managementnya, Banabil harus bisa membuat program yang dapat menerima argumen berupa Jam (0-23), Menit (0-59), Detik (0-59), Tanda asterisk [ * ] (value bebas), serta path file .sh.

- Dalam pelatihan fokus untuk ketepatan pilihannya, Banabil ingin programnya dapat mengeluarkan pesan “error” apabila argumen yang diterima program tidak sesuai. Pesan error dapat dibentuk sesuka hati oleh pembuat program. terserah bagaimana, yang penting tulisan error.

- Terakhir, dalam pelatihan kesempurnaan fokusnya, Banabil ingin program ini berjalan dalam background dan hanya menerima satu config cron.

- Bonus poin apabila CPU state minimum.
Contoh untuk run: /program \* 44 5 /home/Banabil/programcron.sh

***CODE***
```
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>

int main(int argc, char **argv) {
    pid_t child_id;
    int hour, minute, second;
    char *args[100];
    int i = 0;

    if (strcmp(argv[1], "*") != 0) {
        hour = atoi(argv[1]); //ASCII to int dalam lib stdlib.h, ubah char menjadi int
        if (hour < 0 || hour > 23) {
            fprintf(stderr, "Error: Invalid hour argument.\n");
            exit(1);
        }
    }

    if (strcmp(argv[2], "*") != 0) {
        minute = atoi(argv[2]);
        if (minute < 0 || minute > 59) {
            fprintf(stderr, "Error: Invalid minute argument.\n");
            exit(1);
        }
    }

    if (strcmp(argv[3], "*") != 0) {
        second = atoi(argv[3]);
        if (second < 0 || second > 59) {
            fprintf(stderr, "Error: Invalid second argument.\n");
            exit(1);
        }
    }

    //membentuk command untuk dijalankan
    args[i++] = "/bin/bash";
    args[i++] = "-c";
    args[i++] = argv[4];
    args[i] = NULL;

    char *filename = argv[4];
    chmod(filename, S_IRWXU); //memberi hak akses read dan execute

    if (chmod(filename, S_IRWXU) < 0) {
        perror("Error: Failed to set execute permission.");
        exit(1);
    }

    //fork dan jalankan program di background
    child_id = fork();
    if (child_id < 0) {
        fprintf(stderr, "Error: Failed to fork.\n");
        exit(1);
    } 
    else if (child_id == 0) {
        //child process
        time_t now;
        struct tm *tm_now;
        int delay;

        while (1) {
            //mengambil current time
            now = time(NULL);
            tm_now = localtime(&now);

            if (strcmp(argv[1], "*") == 0) {
                //menggunakan semua nilai jam yang mungkin
                hour = tm_now->tm_hour;
            }

            if (strcmp(argv[2], "*") == 0)  {
                //menggunakan semua nilai menit yang mungkin
                minute = tm_now->tm_min;
            }

            if (strcmp(argv[3], "*") == 0)  {
                //menggunakan semua nilai detik yang mungkin
                second = tm_now->tm_sec;
            }

            //menghitung delay berapa detik sampai jadwal
            delay = (hour - tm_now->tm_hour) * 3600 +
                    (minute - tm_now->tm_min) * 60 +
                    (second - tm_now->tm_sec);

            if (delay < 0) {
                //apabila jadwal sudah lewat, hitung ulang untuk besok
                delay += 24 * 3600;
            }

            //menunggu delay selesai
            sleep(delay);

            //menjalankan program
            if (execvp(args[0], args) < 0) {
                fprintf(stderr, "Error: Failed to execute program.\n");
                exit(1);
            }
        }

        exit(0);
    } 
    else {
        wait(NULL);
    }

    return 0;
}

```

Explaination

In this problem, we were tasked to write a program with c language. The program first checks the hour, minute, and second arguments to ensure they are within valid ranges (0-23 for hours, 0-59 for minutes and seconds). The args array is built to store the command that will be executed, including "/bin/bash", "-c", the command from the input argument, and a NULL terminator. The program sets the read and execute permissions for the file containing the command to be executed using chmod(). If there is an error, it prints an error message and exits, then forks a new process to run the command in the background. In the child process, the program enters an infinite loop that calculates the delay (in seconds) until the specified time is reached. If any of the hour, minute, or second arguments are set to "*", the program uses the current time value for that unit. After calculating the delay, the child process sleeps for the duration of the delay. Once the delay is over, the program executes the command using execvp(). If there is an error, it prints an error message and exits. In the parent process, it waits for the child process to complete with wait(NULL).







